from django.db import models
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User

class Category(models.Model):
    name = models.CharField(max_length=20)


    def __str__(self):
        return self.name

class Post(models.Model):
    author = models.CharField(max_length=45)
    create_at = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=100)
    description = models.TextField()
    text = RichTextField()
    banner = models.ImageField(null=True, upload_to='posts')
    category = models.ForeignKey(Category, on_delete=models.SET_NULL,blank=True, null=True, related_name='posts')


    def __str__(self):
        return self.title


class Comment(models.Model):
    text = models.TextField()
    create_at = models.DateTimeField(auto_now_add=True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comments')
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='comments')  

    def __str__(self):
        return self.text






