from django.db.models import Q
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView

from django.shortcuts import redirect

from blog.models import Post, Category, Comment


class LayoutView:

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        return context


class PostListView(LayoutView, ListView):
    model = Post


class CategoryDetailView(LayoutView, DetailView):
    model = Category


class PostDetailView(LayoutView, DetailView):
    model = Post


class SearchView(LayoutView, ListView):
    model = Post
    template_name = 'blog/search.html'

    def get_queryset(self):
        query = self.request.GET.get('q')
        object_list = Post.objects.filter(
            Q(title__icontains=query) |
            Q(text__icontains=query) |
            Q(category__name__icontains=query)
        )
        return object_list


class CommentCreateView(LayoutView, CreateView):
    model = Comment
    fields = ['text']

    def form_valid(self, form):
        post_id = self.kwargs['post']
        self.object = form.save(commit=False)
        self.object.author = self.request.user
        self.object.post = Post.objects.get(id=post_id)
        self.object.save()
        return redirect('post-detail', pk=post_id)


#class PostCategoryListView(ListView):
#    model = Post
#
 #   def get_queryset(self, *args, **kwargs):
  #      queryset = super().get_queryset(*args, **kwargs)
   #     return queryset.filter(category__name='futebol');

#class PostCategorysListView(ListView):
#    model = Post
#
 #   def get_queryset(self, *args, **kwargs):
  #      queryset = super().get_queryset(*args, **kwargs)
   #     return queryset.filter(category__name='sad');


